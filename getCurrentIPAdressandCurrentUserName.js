import { LightningElement ,wire,track} from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id'; 
import NAME_FIELD from '@salesforce/schema/User.Name';
import ipTest from '@salesforce/apex/IpTest.ipTest1';
export default class GetCurrentIPAdressandCurrentUserName extends LightningElement { 
    @track error ;
    @track iptest;
    @track name;
     handleClick(){
        ipTest().then(result=>{
            this.iptest = result;
            console.log(this.iptest);
        }).catch(error=>{
            this.error = error;
        });
    }
     @wire(getRecord, {
         recordId: USER_ID,
         fields: [NAME_FIELD]
     }) wireuser({
         data,
         error
     }) {
         if (data) {
            this.name = data.fields.Name.value;
         } else if (error) {
             this.error = error ;
         }
     }}