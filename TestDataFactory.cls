public with sharing class TestDataFactory {
     public static Account createSingleAccount(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        Account acc = new Account();
        String name;
        if(String.isNotEmpty(firstName))
             name = firstName;
        if(String.isNotEmpty(LastName))
            name = name +' '+ LastName;
        if(String.isNotEmpty(recordTypeName))
            acc.RecordTypeId = recTypeId;
        acc.Name = name;
        insert acc;
        return acc;
    }
    public static List<Account> createAccount(Integer noOfAccounts,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Account> accountList = new List<Account>();
        for(Integer i=0;i<noOfAccounts;i++)
		{
            Account acc = new Account(Name='TestAccount '+i);
            if(String.isNotEmpty(recordTypeName))
             acc.RecordTypeId = recTypeId;
            accountList.add(acc);
        }
        if(accountList.size()>0)
        {
            insert accountList;
        }
        return accountList;
    }

	public static Contact createSingleContact(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        Contact con = new Contact();
        if(String.isNotEmpty(firstName))
             con.firstname = firstName;
        if(String.isNotEmpty(LastName))
            con.lastname = LastName;
        if(String.isNotEmpty(recordTypeName))
            con.RecordTypeId = recTypeId;
        insert con;
        return con;
    }
	public static List<Contact> createContact(Integer noOfContacts,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0;i<noOfContacts;i++)
        {
            Contact con = new Contact(LastName='TestContact '+i);
            if(String.isNotEmpty(recordTypeName))
             con.RecordTypeId = recTypeId;
            contactList.add(con);
        }
        if(contactList.size()>0)
        {
            insert contactList;
        }
        return contactList;
    }

	public static Opportunity createSingleOpportunity(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
		String name;
        Opportunity opp = new Opportunity();
        if(String.isNotEmpty(firstName))
             name = firstName;
        if(String.isNotEmpty(LastName))
            name = name +' '+ LastName;
        if(String.isNotEmpty(recordTypeName))
            opp.RecordTypeId = recTypeId;
        opp.Name = name;
		opp.StageName = 'Prospecting';
		opp.CloseDate = System.today();
        insert opp;
        return opp;
    }
    public static List<Opportunity> createOpportunity(Integer noOfOpportunities,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
            recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Opportunity> oppList = new List<Opportunity>();
        for(Integer i=0;i<noOfOpportunities;i++)
        {
            Opportunity opp = new Opportunity(Name='TestOpportunity '+i,StageName='Prospecting',CloseDate = System.today());
            if(String.isNotEmpty(recordTypeName))
             opp.RecordTypeId = recTypeId;
            oppList.add(opp);
        }
        if(oppList.size()>0)
        {
            insert oppList;
        }
        return oppList;
    }

	public static Lead createSingleLead(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        Lead l = new Lead();
        if(String.isNotEmpty(firstName))
             l.firstname = firstName;
        if(String.isNotEmpty(LastName))
            l.lastname = LastName;
        if(String.isNotEmpty(recordTypeName))
            l.RecordTypeId = recTypeId;
        l.Company = 'MTX';
        insert l;
        return l;
    }
    public static List<Lead> createLead(Integer noOfLeads,String recordTypeName)
    {
        Id recTypeId;
         if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Lead> leadList = new List<Lead>();
        for(Integer i=0;i<noOfLeads;i++)
        {
           Lead l = new Lead();
            l.LastName ='TestLead'+i;
            l.Company='Test';
            if(String.isNotEmpty(recordTypeName))
             l.RecordTypeId = recTypeId;
            leadList.add(l);
        }
        if(leadList.size()>0)
        {
            insert leadList;
        }
        return leadList;
    }
    public static List<User> createUser(Integer noOfUsers)
    {
        Profile pf= [Select Id from profile where Name='System Administrator'][0]; 
        List<User> userList = new List<User>();
        for(Integer i=0;i<noOfUsers;i++)
        {
                User user = new User();
                user.FirstName = 'Test';
                user.LastName = 'Name';
                user.CompanyName = 'IT Test Company';
                user.MobilePhone = '123-456-7890';
                user.Username = 'testUser'+i;
                user.Email = 'testUser'+i+'@test.com';
         		user.Alias = 'test';
                user.CommunityNickname = 'test1';
                user.TimeZoneSidKey = 'America/New_York';
                user.LocaleSidKey = 'en_US';
                user.EmailEncodingKey = 'UTF-8';
                user.ProfileId = pf.Id;
                user.LanguageLocaleKey = 'en_US';
                user.Street = '123 Test St';
                user.City = 'Testcity';
                user.State = 'va';
                user.PostalCode = '23223';
                user.Country = 'USA';
            userList.add(user);
        }
        if(userList.size()>0)
        {
            insert userList;
        }
        return userList;
    }
    public static List<User> createCommunityUser(Integer noOfUsers,Id conId)
    {
        Id profile = [select id from profile where name='Partner Community User'].id;
        List<User> userList = new List<User>();
        for(Integer i=0;i<noOfUsers;i++)
        {
            User user = new User(alias = 'test'+i, email='test'+i+'@noemail.com',
            emailencodingkey='UTF-8', lastname='Test'+i, languagelocalekey='en_US',
            localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
            ContactId = conId,
            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            userList.add(user);
        }
        if(userList.size()>0)
        {
            insert userList;
        }
        return userList;
    }
}
